<?php

abstract class PlayerType {
    const Human = 0;
    const Computer = 1;
}

$minMatchPick = 1;
$maxMatchPick = 3;
$startPlayer = PlayerType::Human;

while (true) {
    $matches = startGame();

    $roundFinished = false;

    // TODO let the player decide whos gets to start the game
    $currentPlayer = $startPlayer;
    while (!$roundFinished) {
        printTurnInformation($currentPlayer);
        $matchesTaken = getPickInput($currentPlayer, $matches, $minMatchPick, $maxMatchPick);

        $matches -= $matchesTaken;

        if ($matches > 0) {
            printRemainingMatches($matches);
        } else {
            printWinningNotification($currentPlayer);
            $roundFinished = true;
            continue;
        }
        $currentPlayer = getNextPlayer($currentPlayer);
    }

    // TODO Neue runde spielen? (J/N)
}

// TODO describe
function startGame() {
    echo "Wie viele Streichhölzer sollen auf dem Tisch liegen? ";
    // TODO Validate input
    $matches = readline();
    return $matches;
}

function getPickInput($player, $matches, $minMatchPick, $maxMatchPick) {
    if($player === PlayerType::Human) {
        echo sprintf("Bitte ziehe zwischen %d und %d Streichhölzer.\n", $minMatchPick, $maxMatchPick);
        $input = readline();
        while (($input < $minMatchPick) || ($input > $maxMatchPick)) {
            echo sprintf("%s ist eine ungültige Eingabe, bitte gib eine Zahl zwischen %d und %d ein. \n",
                $input, $minMatchPick, $maxMatchPick);
            $input = readline();
        }
        echo sprintf("Du hast %s gezogen. \n", getMatchString($input));
        return $input;
    }

    if($player === PlayerType::Computer) {
        $matchesTaken = calculateComputerPick($matches, $minMatchPick, $maxMatchPick);
        echo sprintf("Der Computer hat %s gezogen. \n", getMatchString($matchesTaken));
        return $matchesTaken;
    }
}

// TODO describe
function getMatchString($numMatches) {
    if(abs($numMatches) > 1) {
        return $numMatches . " Streichhölzer";
    }
    return $numMatches . " Streichholz";
}

// TODO describe
function printRemainingMatches($matches) {
    echo sprintf("Es verbleiben %s.\n", getMatchString($matches));
}

function printTurnInformation($player) {
    if($player === PlayerType::Human) {
        echo "------- Der Spieler ist am Zug -------\n";
        return;
    }
    echo "------- Der Computer ist am Zug -------\n";
    return;
}

function printWinningNotification($player) {
    if($player === PlayerType::Human) {
        echo "Du hast gewonnen! \n";
        return;
    }
    echo "Der Computer hat gewonnen! \n";
    return;
}

function getNextPlayer($currentPlayer) {
    if($currentPlayer === PlayerType::Human) {
        return PlayerType::Computer;
    }
    return PlayerType::Human;
}

// TODO describe
function calculateComputerPick($matches, $minMatchPick, $maxMatchPick){
    if ($matches % ($maxMatchPick + 1) == 0) {
        return rand($minMatchPick, $maxMatchPick);
    }
    return max(($matches % ($maxMatchPick + 1)), $minMatchPick);
}

?>